import '../styles/globals.css';
import NavBar from '../components/NavBar';
import Footer from '../components/Footer';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container} from 'react-bootstrap';
import { useState, useEffect } from 'react';
import Head from 'next/head'
//custom css
import '../styles.css'

import { UserProvider } from './../contexts/UserContext'

function MyApp({ Component, pageProps }) {

  const [user, setUser] = useState({ id: null });

  const unsetUser = () => {
    localStorage.clear();
    setUser({id: null})
  }
  useEffect(()=>{
     fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
      headers : {
        Authorization: `Bearer ${localStorage['token']}`
      }
    })
    .then(res => res.json())
    .then( data => {
      if(data._id) {
        setUser({id: data._id })
      } else {
        setUser({ id: null})
      }
    })
  }, [user.id])

    return(
    <>
   
      <UserProvider value={{user, setUser, unsetUser}}>
         <Head>
        <link rel="shortcut icon" href="/favicon.ico" />
      </Head>
	      <NavBar/>
		  <Container>	
		  	<Component {...pageProps} />
		  </Container>
      <Footer/>
	  </UserProvider>
    </>
	) 
}

export default MyApp
