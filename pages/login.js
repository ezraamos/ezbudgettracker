import { useState, useContext } from 'react';
import { Form, Button, Card, Row, Col, Alert, Spinner } from 'react-bootstrap';
import Router from 'next/router';
import Head from 'next/head';

import { GoogleLogin } from 'react-google-login';

import UserContext from './../contexts/UserContext';


const login = () => {

   const { user, setUser } = useContext(UserContext)
   const [isLoading, setIsLoading] = useState(false)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [tokenId, setTokenId] = useState(null)

    const authenticate = (e) => {
        e.preventDefault()
          setIsLoading(true)

        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'  },
            body: JSON.stringify({ email: email, password: password })
        }
        
        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/login`, options).then((response) => response.json()).then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
              alert("something went wrong")
            }
        })
    }
    
    const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: { Authorization: `Bearer ${ accessToken }` } 
        }

        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, options).then((response) => response.json()).then(data => {
            setUser({ id: data._id})
            setIsLoading(false)
            Router.push('/transaction')
        })
    }

    const authenticateGoogleToken = (response) => {
        setTokenId(response.tokenId)

        const payload = {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ tokenId: response.tokenId })
        }

        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/verify-google-id-token`, payload).then((response) => response.json()).then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                alert('something went wrong')
            }
        })
    }



    return (
        <>
            <Head>
                <title>Login</title>
            </Head>

            
                    <Form className="indexForm" onSubmit={e=>authenticate(e)}>
                        <h1>
                            <span className="font-weight-bold">ezbudgettracker</span>.com
                        </h1>
                        <h3 className="text-center"> hello :) </h3>
                        <Form.Group controlId='userEmail'>
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                type="email"
                                value={email}
                                onChange={ e => setEmail(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group controlId='password'>
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                value={password}
                                onChange={ e => setPassword(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Button className="mb-2" variant="dark" type="submit" block>

                                {
                                    isLoading === true ? <Spinner as="span"animation="grow" size="sm" role="status" aria-hidden="true"/>
                                    : ""
                                }
                             Login
                        </Button>
                        <GoogleLogin
                        clientId="860221359432-ko4f6b210brjc8jddelt5rau8dp03fd7.apps.googleusercontent.com"
                        buttonText="Login"
                        onSuccess={ authenticateGoogleToken }
                        onFailure={ authenticateGoogleToken }
                        cookiePolicy={ 'none' }
                        className="w-100 text-center d-flex justify-content-center"
                         />
                    </Form>
        </>
    );
}

export default login;
