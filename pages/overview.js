import React, {useState, useEffect} from 'react'
import Head from 'next/head'
import{Row, Col} from 'react-bootstrap';
import MonthlyTrendBar from './../components/MonthlyTrendBar';
import CategoryPie from './../components/CategoryPie';



export default function trend(){


const [records, setRecords] = useState([])



useEffect(() => {
       fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
       		headers:{
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
       })
       .then( res => res.json())
       .then(data => {

       	if(data._id){
       		setRecords(data.records)
       	}else{
       		setRecords([])
       	}
       })
    }, [])

const incomeTransactions= records.filter(record => record.transactionType === "income");
const expenseTransactions=records.filter(record => record.transactionType === "expense");





	return(
		<>
			<Head>
				<title>Overview</title>
			</Head>
			<Row>
				<Col xs={12} md={2}>
					<div className="title-container">
					<h4>OVERVIEW</h4>
						
					</div>
				</Col>
				<Col xs={12} md={5}>
					<div className="incomeRow">
					<h3 className="overIncome">Income</h3>
						
					</div>
				</Col>
				<Col xs={12} md={5}>
					<div className="expenseRow">
					<h3 className="overExpense">Expense</h3>
						
					</div>
				</Col>
			</Row>

			<hr/>
				
			<MonthlyTrendBar records={records} incomeTransactions={incomeTransactions} expenseTransactions={expenseTransactions}/>
			<hr/>
			<CategoryPie className="categoryPie" incomeTransactions={incomeTransactions} expenseTransactions={expenseTransactions}/>
			<hr/>
		</>
	)
}