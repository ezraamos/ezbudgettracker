import { useState, useEffect} from 'react';
import { Form, Button, Alert, Card, Spinner } from 'react-bootstrap';
import Router from 'next/router'
import Head from 'next/head'

const register = () => {

    const [firstName, setFirsName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
   const [isLoading, setIsLoading] = useState(false)

    const [isActive, setIsActive] = useState(false);

    useEffect( ()=>{

        if( password1 !== '' && password2 !== '' && password1 === password2) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }

    },[password1,password2])

    function registerUser(e) {
        setIsLoading(true)
        e.preventDefault();

        // check if user does not exist
        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/email-exists`, {
            method: "POST",
            headers : {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email : email
            })
        })
        .then( res => res.json())
        .then( data => {
            // if no duplicates found, register the user
            if(data === false) {
                fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users`, {
                    method: "POST",
                    headers : {
                        'Content-Type' : 'application/json'
                    },
                    body: JSON.stringify({
                        firstName,
                        lastName,
                        email,
                        password: password1,
                     
                    })
                })
                .then( res=> res.json())
                .then( data => {
                    if( data === true) {
                        alert('You are registered successfully')
                        setIsLoading(false)
                        Router.push('/login')
                    } else {
                        alert('Something went wrong')
                        
                    }
                })
            } else {
                alert('email already in use')
                
            }
        })
    }

    return (
        <>
            <Head>
                <title>Register</title>
            </Head>

           
                    <Form className="indexForm" onSubmit={ e => registerUser(e)} >
                      <h1>
                            <span className="font-weight-bold">ezbudgettracker</span>.com
                        </h1>
                         <h3 className="text-center">sign up!</h3>
                        <Form.Group controlId="firstName">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control
                                type="firstName"
                              
                                value={firstName}
                                onChange={ e => setFirsName(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group controlId="lastName">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control
                                type="lastName"
                          
                                value={lastName}
                                onChange={ e => setLastName(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <Form.Group controlId="userEmail">
                            <Form.Label>Email Address</Form.Label>
                            <Form.Control
                                type="email"
                              
                                value={email}
                                onChange={ e => setEmail(e.target.value)}
                                required
                            />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else
                            </Form.Text>
                        </Form.Group>
                        
                        <Form.Group controlId="password1">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                value={password1}
                                onChange={ e => setPassword1(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="password2">
                            <Form.Label>Verify Password</Form.Label>
                            <Form.Control
                                type="password"
                                value={password2}
                                onChange={ e => setPassword2(e.target.value)}
                                required
                            />
                        </Form.Group>

                        {
                            isActive ?
                            <Button variant="dark" type="submit" block>
                            {
                                isLoading === true ?
                                  <Spinner
                                      as="span"
                                      animation="grow"
                                      size="sm"
                                      role="status"
                                      aria-hidden="true"
                                    />
                                : ""
                            }

                            Submit</Button> :
                            <Button variant="dark" type="submit" block disabled>Submit</Button>
                        }
                    </Form> 
           
        </>
    );
}

export default register;