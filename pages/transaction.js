import React, {useState, useEffect} from 'react';
import{Row, Col} from 'react-bootstrap'
import Head from 'next/head'
import InputForm from './../components/InputForm';
import Headers from './../components/Headers';
import SearchRecordTab from './../components/SearchRecordTab';
import IncomeExpenseTabs from './../components/IncomeExpenseTabs';


export default function transaction(){

	const [records, setRecords] = useState([]);
	const [rawData, setRawData] = useState([]);

	useEffect(() => {

       fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
       		headers:{
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
       })
       .then( res => res.json())
       .then(data => {

       	if(data._id){

       		setRecords(data.records)
       		setRawData(data)
       	}else{
       		setRecords([])
       	}

       })

       

    }, [])

   

	return(
		<>
			<Head>
				<title>Transaction/Logs</title>
			</Head>
			
			<Headers records={records} rawData={rawData}/>
			<InputForm/>

			<Row>
				<Col xs={12} md={6}>
					<SearchRecordTab records={records}/>
				</Col>
				<Col xs={12} md={6} className="incomeExpenseTabs">
			        <IncomeExpenseTabs records={records}/> 
				</Col>
			</Row>
			<hr/>
			
		</>
	)
}