import Head from 'next/head'
import Banner from '../components/Banner'


export default function Home() {

 const banner = {
    brand: "EverZealous",
    title: " ezBudget Tracker",
    content: "“It is not the man who has too little, but the man who craves more, that is poor”",
    subContent: "-Lucius Annaeus Seneca"

  }

  return (
      <>
        <Head>
          <title>
            Budget Tracker
          </title>
        </Head>
      <Banner banner={banner}/>

      
    </>
  
  )
}
