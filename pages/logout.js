import {useContext, useEffect} from 'react';
import UserContext from './../contexts/UserContext';
import Router from 'next/router';

export default function logout(){
	const {unsetUser} = useContext(UserContext);

	useEffect( () => {
		unsetUser();
		localStorage.clear()
		Router.push('/')
	})

	return null
}