import React from 'react'

const UserContext = React.createContext()

// Context provider component to allow context change subscriptions of child components.
export const UserProvider = UserContext.Provider 

// Context component as default object to be exported.
export default UserContext