import React,  {useState, useEffect} from 'react';
import {Alert, Button, Row, Col, Spinner} from 'react-bootstrap';
import Router from 'next/router';

function InputForm() {

 

  const [transactionType, setTransactionType] = useState('')
  const [category, setCategory] = useState('')
  const [remark, setRemark] = useState('')
  const [amount, setAmount] = useState(0)
  const[isLoading, setIsLoading] = useState(false)

  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if(amount !== 0 && amount !== "" && category !== "" && transactionType !== "" && remark !== ""){
      setIsActive(true)
    }else{
      setIsActive(false)
    }
    
  }, [amount,transactionType, category,remark])



  const addTransaction = () => {

     setIsLoading(true)

    if(transactionType === "expense" || transactionType === "income"  ){
        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/transactions`, {
      method: 'POST',
      headers: {
        'Content-Type' : 'application/json',
        'Authorization' : `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
         transactionType,
         category,
         remark,
         amount
            })
    })
    .then( res => res.json())
    .then( data => {
      console.log(data)
        if (data === true) {
         
          alert('The transaction has been added to your logs')
           setIsLoading(false)
          
          
        } else {
          alert("Something went wrong")
        
        }
      }) 
    
        setTransactionType("")
        setRemark("")
        setCategory("")
        setAmount(0)    
    }else{
        alert("please enter a valid type of transaction")
        
    }
    
    
  }

  return (
    <form className="input-form my-5" onSubmit={addTransaction}>
      <div className="form-inner">
      <Row>
        <Col className="px-0"sm={6} md={3}>
           <input 
              type="text"  
              placeholder="Income or Expense"
              value={transactionType}
              onChange={ e => setTransactionType(e.target.value.toLowerCase())}
            /> 
        </Col>
        <Col className="px-0" sm={6} md={3}>
            <input 
              type="text" 
              maxLength="15" 
              placeholder="Enter a category" 
              value={category}
              onChange={ e => setCategory(e.target.value.toLowerCase())}
             /> 
        </Col>
        <Col className="px-0" sm={6} md={3}>
            <input 
              type="text" 
              maxLength="15"
              placeholder="Make a remark" 
              value={remark}
              onChange={ e => setRemark(e.target.value)}
            /> 
        </Col>
        <Col className="px-0" sm={6} md={3}>
            <input 
              type="number"  
              value={amount}
               placeholder="Amount"
              onChange={ e => setAmount(parseInt(e.target.value))}
             />
        </Col>
      </Row>
      </div>
      <div className="form-inner">
           {isActive === true
         ? <Button className="mx-5" variant="light" type="submit" value="Add Transaction"  >  
          {
            isLoading === true ?  <Spinner
                                      as="span"
                                      animation="grow"
                                      size="sm"
                                      role="status"
                                      aria-hidden="true"
                                   />         
            : ""
          } 
         
          <b> Add Transaction</b></Button>
         : <Button className="mx-5" variant="light" type="submit" value="Add Transaction"  disabled><b> Add Transaction</b></Button>
        }
        </div>
    </form>
  )
}

export default InputForm;