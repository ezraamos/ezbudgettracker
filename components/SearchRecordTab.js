import React, {useState, useEffect} from 'react';
import {Form, Row, Col, Tabs, Tab} from 'react-bootstrap'
import moment from 'moment'

export default function SearchRecord({records}){


const[searchField, setSearchField] = useState('')
const [key, setKey] = useState('searchRecord');



	
	const filteredRecords = records.filter(record =>{
		return record.category.toLowerCase().includes(searchField.toString().toLowerCase())
		})

	return(
			<>
			
				<Form.Group controlId="record" className="my-4">
			<Form.Control
				type="record"
				placeholder="search a transaction by its category"
				value={searchField}
				onChange={ e => setSearchField(e.target.value)}
				required
			/>
		</Form.Group>

			<Tabs>
			<Tab title="Transaction Logs" disabled >
                 
                  </Tab>
			 <Tab eventKey="searchRecord" title="All" >
						   		{filteredRecords.reverse().map(record => {
			
			return(
				<Row>
					<Col>						
						<li key={record._id} className={record.transactionType === "expense" ? 'minus' : 'plus'}>
						<div className="contentContainer">
					      <span className="transaction-text"><b>{record.category}</b></span>
					      <span className="transaction-text">{record.remark}</span>
					      <span className="transaction-amount">${record.amount}     </span>
					      <span className="transaction-date">{moment(record.date).format('MMMM DD, YYYY')}     </span>
						</div>
					    </li>
					</Col>
				</Row>
			)
		})}
					        
					      </Tab>
					</Tabs>
		
		


		
			</>
		)
}