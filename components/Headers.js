import React,{useState, useEffect, useContext} from 'react'
import {Row, Col} from 'react-bootstrap'



export default function Headers({records, rawData}){
	

    const incomeTransactions = records.filter(record => record.transactionType === "income");
    const incomeAmounts = incomeTransactions.map(record => record.amount)

    const expenseTransactions = records.filter(record => record.transactionType === "expense");
    const expenseAmounts = expenseTransactions.map(record => record.amount)

    const totalIncome = incomeAmounts.reduce((acc,item)=> (acc += item),0).toFixed(2)
    const totalExpense = expenseAmounts.reduce((acc,item)=> (acc += item),0).toFixed(2)

    const balance = (totalIncome - totalExpense).toFixed(2)


	return(
		<>
		<h1 className='transactionTitle text-center display-4 pt-2 pb-4'>ezBudget Tracker</h1>
		<Row>
			<Col xs={12} md={4}>
				<div className="avatar-container">
					<div>
						<img className="avatar" src={`https://robohash.org/${rawData._id}`} alt="profpic"/>
					</div>		
					<div className="avatarDetails text-center">	
						<h5 className="fullNameAvatar"><b>{rawData.firstName} {rawData.lastName}</b></h5>
						<h6>{rawData.email}</h6>				
					</div>
				</div>
			</Col>

			<Col xs={12} md={4}>
				<div className="inc-exp-container">
			        <div>
			          <h4>Income</h4>
			 		  <p className="money plus">${totalIncome}</p>
			        </div>
			        <div>
			          <h4>Expense</h4>
			  		  <p className="money minus">${totalExpense}</p>
			        </div>
			      </div>
			</Col>
			<Col xs={12} md={4}>
				<div className="balance-container">
			      <h4>Your Balance</h4>
    			  <h1>${balance}</h1>
    			</div>  
			</Col>
			
		</Row>
		</>

	)
}