import React, { useState, useEffect } from 'react';
import { Row, Col } from 'react-bootstrap';
import { Pie } from 'react-chartjs-2';
import { colorRandomizer } from '../helpers';


export default function CategoryPie({incomeTransactions, expenseTransactions}){

	const [categoryIncs, setCategoryIncs] = useState([])
	const [categoryExps, setCategoryExps] = useState([])

	const [amountIncs, setAmountIncs] = useState([])
	const [amountExps, setAmountExps] = useState([])

	const [bgColorsInc, setBgColorsInc] = useState([])
	const [bgColorsExp, setBgColorsExp] = useState([])

	useEffect(() => {
			let incCats = []
			incomeTransactions.forEach( tran => {
				if(!incCats.find(cat => cat === tran.category)){
					incCats.push(tran.category)
				}
			})
			setCategoryIncs(incCats)

		let expCats = []
			expenseTransactions.forEach( tran => {
				if(!expCats.find(cat => cat === tran.category)){
					expCats.push(tran.category)
				}
			})
			setCategoryExps(expCats)
		  },[incomeTransactions, expenseTransactions])

	useEffect( ()=> {
        setAmountIncs(categoryIncs.map(cat => {
            let amountIncs = 0;
            incomeTransactions.forEach(tran => {
                if(tran.category === cat){
                    amountIncs = amountIncs + parseInt(tran.amount)
                }
            })
            return amountIncs;
        }))

        setBgColorsInc(categoryIncs.map(()=> `#${colorRandomizer()}`))


         setAmountExps(categoryExps.map(cat => {
            let amountExps = 0;
            expenseTransactions.forEach(tran => {
                if(tran.category === cat){
                    amountExps = amountExps + parseInt(tran.amount)
                }
            })
            return amountExps;
        }))

        setBgColorsExp(categoryExps.map(()=> `#${colorRandomizer()}`))
    },[categoryExps])

		// if(incomeTransactions.length > 0){
		
		// 	 setCategoryIncs(incomeTransactions.map(element => element.category))
		// 	setAmountIncs(incomeTransactions.map(element => element.amount))
			
		// 	setBgColorsInc(incomeTransactions.map(() => `#${colorRandomizer()}`))
		// }
		// if(expenseTransactions.length > 0){
		
		// 	 setCategoryExps(expenseTransactions.map(element => element.category))
		// 	setAmountExps(expenseTransactions.map(element => element.amount))
			
		// 	setBgColorsExp(expenseTransactions.map(() => `#${colorRandomizer()}`))
		// }
	// }, [incomeTransactions])

	const dataInc ={
		labels: categoryIncs, 
		datasets:[{
			data: amountIncs,
			backgroundColor: bgColorsInc,
			hoverBackgroundColor: bgColorsInc
		}]
	}

	const dataExp ={
		labels: categoryExps, 
		datasets:[{
			data: amountExps,
			backgroundColor: bgColorsExp,
			hoverBackgroundColor: bgColorsExp
		}]
	}


	return(
		<>	
			<Row>
				<Col xs={12} md={2}>
					<div className="title-container">
						<h4 className="lead">Breakdown of Categories</h4>
						
					</div>
				</Col>
				<Col xs={12} md={5}><Pie data={dataInc}/></Col>
				{<Col xs={12} md={5}><Pie data={dataExp}/></Col>}
			</Row>			
		</>
	)
}