
import {Table, Row, Col, Alert, Tabs} from 'react-bootstrap'
import moment from 'moment'

export default function IncomeLogs({records}) {

const incomeRecords = records;
  

   
	return(
	<>
		
		{incomeRecords.reverse().map(record => {
			
			return(
				<Row>
					<Col>						
						<li key={record._id} className='plus'>
						<div className="contentContainer">
					      <span className="transaction-text"><b>{record.category}</b></span>
					      <span className="transaction-text">{record.remark}</span>
					      <span className="transaction-amount">${record.amount}     </span>
					      <span className="transaction-date">{moment(record.date).format('MMMM DD, YYYY')}     </span>
						</div>
					    </li>
					</Col>
				</Row>
			)
		})}
	</>
	)
}