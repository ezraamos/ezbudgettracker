import React,{useState, useEffect} from 'react'
import {Tabs, Tab, Button} from 'react-bootstrap'

import IncomeLogs from './IncomeLogs'
import ExpenseLogs from './ExpenseLogs'




export default function IncomeExpenseTabs({records}){
	const [key, setKey] = useState('transactionLogs');



  const incomeTransactions = records.filter(record => record.transactionType === "income");
  const expenseTransactions = records.filter(record => record.transactionType === "expense");


	return(
		<>

		<Tabs
			activeKey={key}
			onSelect={(k) => setKey(k)}
		>	  
			<Tab eventKey="income" title="Income">
				<IncomeLogs records={incomeTransactions} />
			</Tab>
				<Tab eventKey="expense" title="Expense">
			<ExpenseLogs records={expenseTransactions} />
			</Tab>
		</Tabs>
		</>
		)
	}