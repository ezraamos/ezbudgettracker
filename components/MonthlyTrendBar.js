import { useState, useEffect } from 'react';
import { Row, Col } from 'react-bootstrap'
import { Bar } from 'react-chartjs-2';
import moment from 'moment';

export default function MonthlyTrendBar({records, incomeTransactions, expenseTransactions}) {

	const [months, setMonths] = useState([])
	const [monthlyIncome, setMonthlyIncome] = useState([])
	const [monthlyExpense, setMonthlyExpense] = useState([])

	useEffect(() => {
		if(records.length > 0) {
			let tempMonths = [] //placeholderarray for each distinct month from rawData
			records.forEach(record => {
				//check for duplicates to ensure months recorder are distinct
				if(!tempMonths.find(month => month === moment(record.date).format('MMMM'))){
					tempMonths.push(moment(record.date).format('MMMM'))
				}
			})

			const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

			tempMonths.sort((a,b) => {
				//if both months being compaired are found in the reference array 
				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1) {
					//get the difference of their indices, a negative result will place the first elemet before b, ande vice versa
					return monthsRef.indexOf(a) - monthsRef.indexOf(b)
				}
			})
			setMonths(tempMonths)
		}
	}, [records])


	
	useEffect(() => {
		setMonthlyIncome(months.map(month => {
			let amount = 0
			incomeTransactions.forEach(record => {
				if(moment(record.date).format('MMMM') === month){
					amount = amount + parseInt(record.amount)
				}
			})
			return amount
		}))
	}, [months])

	useEffect(() => {
		setMonthlyExpense(months.map(month => {
			let amount = 0
			expenseTransactions.forEach(record => {
				if(moment(record.date).format('MMMM') === month){
					amount = amount + parseInt(record.amount)
				}
			})
			return amount
		}))
	}, [months])

	const data1 = {
		labels: months,
		datasets: [{
			label: 'Monthly Income in US$',
			backgroundColor:'rgba(84, 246, 194, 0.2)',
			borderColor: 'rgba(72, 212, 128, 1)',
			borderWidth: '1',
			hoverBackgroundColor:'rgba(72, 212, 128, 0.4)',
			borderColor: 'rgba(84, 246, 194, 1)',
			data: monthlyIncome

		}]
	}

	const data2 = {
		labels: months,
		datasets: [{
			label: 'Monthly Expense in US$',
			backgroundColor: 'rgba(255,99,132,0.2)' ,
			borderColor:'rgba(255,99,132,1)' ,
			borderWidth: '1',
			hoverBackgroundColor: 'rgba(255,99,132,0.4)' ,
			borderColor:'rgba(255,99,132,1)' ,
			data: monthlyExpense

		}]
	}



	return(

		<Row>
			<Col xs={12} md={2}>
					<div className="title-container">
						<h4 className="lead">Monthly Trends</h4>
					</div>
				</Col>
			<Col xs={12} md={5}>
				<Bar data={data1}/>
			</Col>
			<Col xs={12} md={5}>
				<Bar data={data2}/>
			</Col>
		</Row>
		
		
	)
}