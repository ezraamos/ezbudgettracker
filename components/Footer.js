import React from "react";
import {Row, Col, Container} from 'react-bootstrap';

function Footer() {
  return (
    <>
     <div className="superfooter">
         <h3 className="websiteFooter"><b>ezbudgettracker</b>.com</h3>

      <Container>

    
        <Row>
    
          <Col xs={12} md={4}>
            <h3 className="lead"><em>EVERZEALOUS inc.</em></h3>
            <ul className="list-unstyled">
              <li>32-420-6969</li>
              <li>Somewhere, OverThere</li>
              <li>123 Street TimogSilangan</li>
            </ul>
          </Col>
   
          <Col xs={12} md={4}>
              <h3 className="lead"><em>Stuff</em></h3>
            <ul className="list-unstyled">
              <li>gutenmorgen- words</li>
              <li>amboot- some words</li>
              <li>ayo way ayo - more words</li>
            </ul>
          </Col>
        
          <Col xs={12} md={4}>
             <h3 className="lead"><em>Other Applications</em></h3>
            <ul className="list-unstyled">
              <li><b>something</b>.com</li>
              <li><b>lahinga</b>.com</li>
              <li><b>lahinapudnga</b>.com</li>
            </ul>
          </Col>

        </Row>
      
        
          <p className="containerFooter">
            &copy;{new Date().getFullYear()} EVER ZEALOUS| All rights reserved |
            Terms Of Service | Privacy
          </p>
       
      </Container>
    </div>
   
      

    </>
  );
}

export default Footer;