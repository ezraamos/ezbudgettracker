import {Jumbotron} from 'react-bootstrap';

export default function Banner({banner}){
	return(
		<Jumbotron className="jumbotron text-left">
        
             <h1 className="display-4" style={{'fontSize': '35px'}}> {banner.brand}</h1>

             <div className='my-5'>
               
             
             <h1 className="indexTitle display-1 mt-5" style={{'fontSize': '50px'}}> <strong><span className="bannerTitle">{banner.title}</span></strong> </h1>
        
        <p className='lead'style={{'fontSize': '20px'}}>
        	
        		{banner.content}    
        </p>
        <p style={{'fontSize': '15px'}}>
          
            {banner.subContent}    
        </p>
             </div>

       
      </Jumbotron>

 
		)

}
