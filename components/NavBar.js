import { Navbar, Nav } from 'react-bootstrap'
import Link from 'next/link'
import UserContext from './../contexts/UserContext';
import { useContext } from 'react';

const NavBar = () => {

    const {user} = useContext(UserContext)

    return (
        <Navbar className="shadow-sm p-3 mb-5 bg-white rounded" bg="white" variant="light" expand="lg">
            <Link href="/">
                <a className="navbar-brand"><img className="navlogo" src="./../brand2.png"/></a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto">
                    {user.id !== null ?
                        <>
                            <Link href="/transaction">
                                <a className="nav-link" role="button">Transaction</a>
                            </Link>
                            <Link href="/overview">
                                <a className="nav-link" role="button">Overview</a>
                            </Link>
                        
                            <Link href="/logout">
                                <a className="nav-link" role="button">Logout</a>
                            </Link>
                        </>
                    :
                        <>
                            <Link href="/login">
                                <a className="nav-link" role="button">Login</a>
                            </Link>
                            <Link href="/register">
                                <a className="nav-link" role="button">Register</a>
                            </Link>
                        </>
                    }
                    
                </Nav>
            </Navbar.Collapse>
        </Navbar>

       
    );
}

export default NavBar;